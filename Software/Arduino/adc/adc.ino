void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Setup succeeded");
}

void loop() {
  // put your main code here, to run repeatedly:
  int val = analogRead(PA0);
  
  float voltage = (float(val)/1024) * 3.3; //formulae to convert the ADC value to voltage
  Serial.print("Voltage = ");// read the ADC value from pin PA
  Serial.print(voltage);
  Serial.println(" Volt");
  delay(500);
}
