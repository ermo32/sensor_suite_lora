EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x06_Female J?
U 1 1 5F7F9DF5
P 5950 3800
AR Path="/5F7F9DF5" Ref="J?"  Part="1" 
AR Path="/5F7F4ED4/5F7F9DF5" Ref="J4"  Part="1" 
F 0 "J4" H 5978 3776 50  0000 L CNN
F 1 "BME/P280" H 5978 3685 50  0000 L CNN
F 2 "" H 5950 3800 50  0001 C CNN
F 3 "~" H 5950 3800 50  0001 C CNN
	1    5950 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3600 5750 3600
$Comp
L power:GND #PWR?
U 1 1 5F7F9E03
P 4700 3800
AR Path="/5F7F9E03" Ref="#PWR?"  Part="1" 
AR Path="/5F7F4ED4/5F7F9E03" Ref="#PWR0134"  Part="1" 
F 0 "#PWR0134" H 4700 3550 50  0001 C CNN
F 1 "GND" H 4705 3627 50  0000 C CNN
F 2 "" H 4700 3800 50  0001 C CNN
F 3 "" H 4700 3800 50  0001 C CNN
	1    4700 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3800 5350 3800
Wire Wire Line
	5350 3800 5350 4250
Wire Wire Line
	5350 4250 4200 4250
Text HLabel 4200 4250 0    50   Input ~ 0
SCL_PB6
Wire Wire Line
	5750 3900 5450 3900
Wire Wire Line
	5450 3900 5450 4350
Wire Wire Line
	5450 4350 4200 4350
Text HLabel 4200 4350 0    50   BiDi ~ 0
SDA_PB7
$Comp
L Device:C C23
U 1 1 5F7FAAB6
P 4700 3500
F 0 "C23" H 4815 3546 50  0000 L CNN
F 1 "100n" H 4815 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4738 3350 50  0001 C CNN
F 3 "~" H 4700 3500 50  0001 C CNN
F 4 "C14663" H 4700 3500 50  0001 C CNN "LCSC Part #"
F 5 "C14663" H 4700 3500 50  0001 C CNN "LCSC"
	1    4700 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3700 4700 3650
Wire Wire Line
	4700 3700 5750 3700
Wire Wire Line
	5200 3600 5200 3300
Wire Wire Line
	5200 3300 4700 3300
Wire Wire Line
	4700 3300 4700 3350
Wire Wire Line
	4700 3800 4700 3700
Connection ~ 4700 3700
Connection ~ 4700 3300
Wire Wire Line
	4700 3300 4200 3300
Text HLabel 4200 3300 0    50   Input ~ 0
P3V3
$EndSCHEMATC
