EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L hx711:HX711 U10
U 1 1 5F8BC3FA
P 6200 3850
F 0 "U10" H 5850 4600 50  0000 C CNN
F 1 "HX711" H 6200 3950 50  0000 C CNN
F 2 "project-footprints:hx711" H 6200 3900 50  0001 L BNN
F 3 "SparkFun" H 6200 3900 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 6200 3900 50  0001 L BNN "Field4"
	1    6200 3850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
