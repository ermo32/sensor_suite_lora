EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3850 3950 5200 3950
Wire Wire Line
	3850 4100 5200 4100
Text HLabel 3850 3950 0    50   Input ~ 0
GPS_TX
Text HLabel 3850 4100 0    50   Output ~ 0
GPS_RX
Wire Wire Line
	3850 3800 5200 3800
Text HLabel 3850 3800 0    50   Input ~ 0
P5V0
Text HLabel 5200 3950 2    50   Output ~ 0
GPS_TX_O
Text HLabel 5200 4100 2    50   Input ~ 0
GPS_RX_I
$EndSCHEMATC
