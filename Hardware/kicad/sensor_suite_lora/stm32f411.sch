EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C?
U 1 1 5F85BD5B
P 7100 1500
AR Path="/5F85BD5B" Ref="C?"  Part="1" 
AR Path="/5F823278/5F85BD5B" Ref="C15"  Part="1" 
F 0 "C15" H 7215 1546 50  0000 L CNN
F 1 "100n" H 7215 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7138 1350 50  0001 C CNN
F 3 "~" H 7100 1500 50  0001 C CNN
F 4 "C14663" H 7100 1500 50  0001 C CNN "LCSC Part #"
F 5 "C14663" H 7100 1500 50  0001 C CNN "LCSC"
	1    7100 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F85BD61
P 7600 1500
AR Path="/5F85BD61" Ref="C?"  Part="1" 
AR Path="/5F823278/5F85BD61" Ref="C16"  Part="1" 
F 0 "C16" H 7715 1546 50  0000 L CNN
F 1 "100n" H 7715 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7638 1350 50  0001 C CNN
F 3 "~" H 7600 1500 50  0001 C CNN
F 4 "C14663" H 7600 1500 50  0001 C CNN "LCSC Part #"
F 5 "C14663" H 7600 1500 50  0001 C CNN "LCSC"
	1    7600 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F85BD67
P 8100 1500
AR Path="/5F85BD67" Ref="C?"  Part="1" 
AR Path="/5F823278/5F85BD67" Ref="C17"  Part="1" 
F 0 "C17" H 8215 1546 50  0000 L CNN
F 1 "100n" H 8215 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8138 1350 50  0001 C CNN
F 3 "~" H 8100 1500 50  0001 C CNN
F 4 "C14663" H 8100 1500 50  0001 C CNN "LCSC Part #"
F 5 "C14663" H 8100 1500 50  0001 C CNN "LCSC"
	1    8100 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F85BD6D
P 8600 1500
AR Path="/5F85BD6D" Ref="C?"  Part="1" 
AR Path="/5F823278/5F85BD6D" Ref="C18"  Part="1" 
F 0 "C18" H 8715 1546 50  0000 L CNN
F 1 "100n" H 8715 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8638 1350 50  0001 C CNN
F 3 "~" H 8600 1500 50  0001 C CNN
F 4 "C14663" H 8600 1500 50  0001 C CNN "LCSC Part #"
F 5 "C14663" H 8600 1500 50  0001 C CNN "LCSC"
	1    8600 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 1150 8600 1350
Wire Wire Line
	8100 1350 8100 1150
Connection ~ 8100 1150
Wire Wire Line
	8100 1150 8600 1150
Wire Wire Line
	7600 1350 7600 1150
Connection ~ 7600 1150
Wire Wire Line
	7600 1150 8100 1150
Wire Wire Line
	7100 1150 7100 1350
Wire Wire Line
	7100 1150 7600 1150
$Comp
L power:GND #PWR?
U 1 1 5F85BD7C
P 7100 1750
AR Path="/5F85BD7C" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F85BD7C" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 7100 1500 50  0001 C CNN
F 1 "GND" H 7105 1577 50  0000 C CNN
F 2 "" H 7100 1750 50  0001 C CNN
F 3 "" H 7100 1750 50  0001 C CNN
	1    7100 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F85BD82
P 7600 1750
AR Path="/5F85BD82" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F85BD82" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 7600 1500 50  0001 C CNN
F 1 "GND" H 7605 1577 50  0000 C CNN
F 2 "" H 7600 1750 50  0001 C CNN
F 3 "" H 7600 1750 50  0001 C CNN
	1    7600 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F85BD88
P 8100 1750
AR Path="/5F85BD88" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F85BD88" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 8100 1500 50  0001 C CNN
F 1 "GND" H 8105 1577 50  0000 C CNN
F 2 "" H 8100 1750 50  0001 C CNN
F 3 "" H 8100 1750 50  0001 C CNN
	1    8100 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F85BD8E
P 8600 1750
AR Path="/5F85BD8E" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F85BD8E" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 8600 1500 50  0001 C CNN
F 1 "GND" H 8605 1577 50  0000 C CNN
F 2 "" H 8600 1750 50  0001 C CNN
F 3 "" H 8600 1750 50  0001 C CNN
	1    8600 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 1750 7100 1650
Wire Wire Line
	7600 1750 7600 1650
Wire Wire Line
	8100 1650 8100 1750
Wire Wire Line
	8600 1650 8600 1750
Text GLabel 5150 2500 0    50   Input ~ 0
BOOT0
$Comp
L MCU_ST_STM32F4:STM32F411RETx U?
U 1 1 5F85BDA6
P 5850 3800
AR Path="/5F85BDA6" Ref="U?"  Part="1" 
AR Path="/5F823278/5F85BDA6" Ref="U4"  Part="1" 
F 0 "U4" H 5150 1950 50  0000 C CNN
F 1 "STM32F411RET6" H 5400 1850 50  0000 C CNN
F 2 "Package_QFP:LQFP-64_10x10mm_P0.5mm" H 5250 2100 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00115249.pdf" H 5850 3800 50  0001 C CNN
F 4 "C94355" H 5850 3800 50  0001 C CNN "LCSC"
	1    5850 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2100 5750 1900
Wire Wire Line
	5750 1900 5850 1900
Connection ~ 6050 1900
Wire Wire Line
	5950 2100 5950 1900
Connection ~ 5950 1900
Wire Wire Line
	5950 1900 6050 1900
Wire Wire Line
	5850 2100 5850 1900
Connection ~ 5850 1900
Wire Wire Line
	5850 1900 5950 1900
Wire Wire Line
	5750 5600 5750 5900
Wire Wire Line
	6050 5900 6050 5600
Wire Wire Line
	5850 5600 5850 5900
Wire Wire Line
	5750 5900 5850 5900
Connection ~ 5850 5900
Wire Wire Line
	5850 5900 5950 5900
Wire Wire Line
	5950 5600 5950 5900
Connection ~ 5950 5900
Wire Wire Line
	5950 5900 6050 5900
$Comp
L power:GND #PWR?
U 1 1 5F85BDCF
P 6150 5900
AR Path="/5F85BDCF" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F85BDCF" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0120" H 6150 5650 50  0001 C CNN
F 1 "GND" H 6155 5727 50  0000 C CNN
F 2 "" H 6150 5900 50  0001 C CNN
F 3 "" H 6150 5900 50  0001 C CNN
	1    6150 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 5900 6150 5900
Connection ~ 6050 5900
Text GLabel 5150 5200 0    50   Input ~ 0
LED
Connection ~ 7100 1150
$Comp
L Device:Crystal Y?
U 1 1 5F86A3AC
P 4000 5600
AR Path="/5F86A3AC" Ref="Y?"  Part="1" 
AR Path="/5F823278/5F86A3AC" Ref="Y2"  Part="1" 
F 0 "Y2" H 3850 5700 50  0000 C CNN
F 1 "32.768 kHz" H 4050 5300 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_3215-2Pin_3.2x1.5mm" H 4000 5600 50  0001 C CNN
F 3 "~" H 4000 5600 50  0001 C CNN
F 4 "C48615" H 4000 5600 50  0001 C CNN "LCSC Part #"
F 5 "C48615" H 4000 5600 50  0001 C CNN "LCSC"
	1    4000 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F86A3B2
P 4350 3900
AR Path="/5F86A3B2" Ref="C?"  Part="1" 
AR Path="/5F823278/5F86A3B2" Ref="C13"  Part="1" 
F 0 "C13" H 4465 3946 50  0000 L CNN
F 1 "12p" H 4465 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4388 3750 50  0001 C CNN
F 3 "~" H 4350 3900 50  0001 C CNN
F 4 "C38523" H 4350 3900 50  0001 C CNN "LCSC Part #"
F 5 "C38523" H 4350 3900 50  0001 C CNN "LCSC"
	1    4350 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F86A3B8
P 3650 5750
AR Path="/5F86A3B8" Ref="C?"  Part="1" 
AR Path="/5F823278/5F86A3B8" Ref="C12"  Part="1" 
F 0 "C12" H 3765 5796 50  0000 L CNN
F 1 "4p3" H 3765 5705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3688 5600 50  0001 C CNN
F 3 "~" H 3650 5750 50  0001 C CNN
F 4 "C16148" H 3650 5750 50  0001 C CNN "LCSC Part #"
F 5 "C16148" H 3650 5750 50  0001 C CNN "LCSC"
	1    3650 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F86A3BE
P 4350 5750
AR Path="/5F86A3BE" Ref="C?"  Part="1" 
AR Path="/5F823278/5F86A3BE" Ref="C14"  Part="1" 
F 0 "C14" H 4465 5796 50  0000 L CNN
F 1 "4p3" H 4465 5705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4388 5600 50  0001 C CNN
F 3 "~" H 4350 5750 50  0001 C CNN
F 4 "C16148" H 4350 5750 50  0001 C CNN "LCSC Part #"
F 5 "C16148" H 4350 5750 50  0001 C CNN "LCSC"
	1    4350 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F86A3C4
P 3650 3900
AR Path="/5F86A3C4" Ref="C?"  Part="1" 
AR Path="/5F823278/5F86A3C4" Ref="C11"  Part="1" 
F 0 "C11" H 3765 3946 50  0000 L CNN
F 1 "12p" H 3765 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3688 3750 50  0001 C CNN
F 3 "~" H 3650 3900 50  0001 C CNN
F 4 "C38523" H 3650 3900 50  0001 C CNN "LCSC Part #"
F 5 "C38523" H 3650 3900 50  0001 C CNN "LCSC"
	1    3650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3750 4350 3750
Wire Wire Line
	3650 3750 3850 3750
$Comp
L power:GND #PWR?
U 1 1 5F86A3CC
P 3650 4050
AR Path="/5F86A3CC" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F86A3CC" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 3650 3800 50  0001 C CNN
F 1 "GND" H 3655 3877 50  0000 C CNN
F 2 "" H 3650 4050 50  0001 C CNN
F 3 "" H 3650 4050 50  0001 C CNN
	1    3650 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F86A3D2
P 4350 4050
AR Path="/5F86A3D2" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F86A3D2" Ref="#PWR0122"  Part="1" 
F 0 "#PWR0122" H 4350 3800 50  0001 C CNN
F 1 "GND" H 4355 3877 50  0000 C CNN
F 2 "" H 4350 4050 50  0001 C CNN
F 3 "" H 4350 4050 50  0001 C CNN
	1    4350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 5600 4350 5600
Wire Wire Line
	3850 5600 3650 5600
$Comp
L power:GND #PWR?
U 1 1 5F86A3DA
P 3650 5900
AR Path="/5F86A3DA" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F86A3DA" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 3650 5650 50  0001 C CNN
F 1 "GND" H 3655 5727 50  0000 C CNN
F 2 "" H 3650 5900 50  0001 C CNN
F 3 "" H 3650 5900 50  0001 C CNN
	1    3650 5900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F86A3E0
P 4350 5900
AR Path="/5F86A3E0" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F86A3E0" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 4350 5650 50  0001 C CNN
F 1 "GND" H 4355 5727 50  0000 C CNN
F 2 "" H 4350 5900 50  0001 C CNN
F 3 "" H 4350 5900 50  0001 C CNN
	1    4350 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F86A3E7
P 1350 6950
AR Path="/5F86A3E7" Ref="R?"  Part="1" 
AR Path="/5F823278/5F86A3E7" Ref="R5"  Part="1" 
F 0 "R5" V 1250 6850 50  0000 L CNN
F 1 "180" V 1450 6850 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1280 6950 50  0001 C CNN
F 3 "~" H 1350 6950 50  0001 C CNN
	1    1350 6950
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 6950 1650 6950
Text GLabel 1650 6950 2    50   Output ~ 0
LED
$Comp
L Device:LED D?
U 1 1 5F86A3EF
P 1050 6700
AR Path="/5F86A3EF" Ref="D?"  Part="1" 
AR Path="/5F823278/5F86A3EF" Ref="D4"  Part="1" 
F 0 "D4" V 1089 6582 50  0000 R CNN
F 1 "STATUS BLUE" V 998 6582 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1050 6700 50  0001 C CNN
F 3 "~" H 1050 6700 50  0001 C CNN
F 4 "C72041" H 1050 6700 50  0001 C CNN "LCSC Part #"
F 5 "C72041" H 1050 6700 50  0001 C CNN "LCSC"
	1    1050 6700
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F86A3F5
P 1050 6450
AR Path="/5F86A3F5" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F86A3F5" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 1050 6300 50  0001 C CNN
F 1 "+3.3V" H 1065 6623 50  0000 C CNN
F 2 "" H 1050 6450 50  0001 C CNN
F 3 "" H 1050 6450 50  0001 C CNN
	1    1050 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 6450 1050 6550
Wire Wire Line
	1050 6850 1050 6950
Wire Wire Line
	1050 6950 1200 6950
Wire Wire Line
	4350 3500 4350 3750
Connection ~ 4350 3750
Wire Wire Line
	4350 5400 4350 5600
Connection ~ 4350 5600
Wire Wire Line
	4350 3500 4600 3500
Wire Wire Line
	3650 3400 3650 3750
Connection ~ 3650 3750
Wire Wire Line
	3650 5300 3650 5600
Connection ~ 3650 5600
Wire Wire Line
	6050 1150 6600 1150
Wire Wire Line
	5150 5300 3650 5300
Wire Wire Line
	5150 5400 4350 5400
Wire Wire Line
	5150 3400 3650 3400
Wire Wire Line
	6550 2500 7950 2500
Text HLabel 7950 2500 2    50   Output ~ 0
RESET_PA2
Wire Wire Line
	6550 2700 7950 2700
Text HLabel 7950 2700 2    50   Output ~ 0
NSS_PA4
Wire Wire Line
	6550 2900 7950 2900
Text HLabel 7950 2900 2    50   Output ~ 0
MISO_PA6
Wire Wire Line
	6550 2800 7950 2800
Text HLabel 7950 2800 2    50   Output ~ 0
SCK_PA5
Wire Wire Line
	6550 3000 7950 3000
Text HLabel 7950 3000 2    50   Input ~ 0
MOSI_PA7
Wire Wire Line
	6550 2600 7950 2600
Text HLabel 7950 2600 2    50   Input ~ 0
DIO0_PA3
Text HLabel 7950 2400 2    50   Input ~ 0
DIO1_PB0
Wire Wire Line
	6050 1150 3650 1150
Connection ~ 6050 1150
Text HLabel 2300 1150 0    50   Input ~ 0
P3V3
$Comp
L Device:C C?
U 1 1 5F91670C
P 3650 2500
AR Path="/5F91670C" Ref="C?"  Part="1" 
AR Path="/5F823278/5F91670C" Ref="C10"  Part="1" 
F 0 "C10" H 3765 2546 50  0000 L CNN
F 1 "1u" H 3765 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3688 2350 50  0001 C CNN
F 3 "~" H 3650 2500 50  0001 C CNN
F 4 "C28323" H 3650 2500 50  0001 C CNN "LCSC Part #"
F 5 "C28323" H 3650 2500 50  0001 C CNN "LCSC"
	1    3650 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F916712
P 3650 2000
AR Path="/5F916712" Ref="R?"  Part="1" 
AR Path="/5F823278/5F916712" Ref="R8"  Part="1" 
F 0 "R8" H 3720 2046 50  0000 L CNN
F 1 "10k" H 3720 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3580 2000 50  0001 C CNN
F 3 "~" H 3650 2000 50  0001 C CNN
	1    3650 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F91671E
P 3650 2800
AR Path="/5F91671E" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F91671E" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 3650 2550 50  0001 C CNN
F 1 "GND" H 3655 2627 50  0000 C CNN
F 2 "" H 3650 2800 50  0001 C CNN
F 3 "" H 3650 2800 50  0001 C CNN
	1    3650 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2150 3650 2300
Wire Wire Line
	3650 2650 3650 2800
Connection ~ 3650 2300
Wire Wire Line
	3650 2300 3650 2350
Wire Wire Line
	3650 2300 5150 2300
Wire Wire Line
	3650 1150 3650 1850
Connection ~ 3650 1150
Wire Wire Line
	3650 1150 2300 1150
Wire Wire Line
	6550 4100 7950 4100
Text HLabel 7950 4100 2    50   BiDi ~ 0
TEMP_PB1
$Comp
L Device:R R6
U 1 1 5F93A830
P 1750 3600
F 0 "R6" V 1650 3500 50  0000 L CNN
F 1 "100k" V 1850 3500 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1680 3600 50  0001 C CNN
F 3 "~" H 1750 3600 50  0001 C CNN
	1    1750 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	1900 3600 2000 3600
Text GLabel 2000 3600 2    50   Output ~ 0
BOOT0
Wire Wire Line
	7950 2400 6550 2400
Wire Wire Line
	6550 2300 7950 2300
Text HLabel 7950 2300 2    50   Input ~ 0
PS_SENSE
Wire Wire Line
	6550 3200 7950 3200
Text HLabel 7950 3200 2    50   Output ~ 0
UART_TX_PA9
Wire Wire Line
	6550 3300 7950 3300
Text HLabel 7950 3300 2    50   Input ~ 0
UART_RX_PA10
Wire Wire Line
	6550 4600 7950 4600
Text HLabel 7950 4600 2    50   Output ~ 0
SCL_PB6
Wire Wire Line
	6550 4700 7950 4700
Text HLabel 7950 4700 2    50   BiDi ~ 0
SDA_PB7
Wire Wire Line
	6550 3400 7950 3400
Wire Wire Line
	6550 3500 7950 3500
Text HLabel 7950 3400 2    50   Output ~ 0
GPS_TX
Text HLabel 7950 3500 2    50   Input ~ 0
GPS_RX
Wire Wire Line
	6550 3600 7950 3600
Wire Wire Line
	6550 3700 7950 3700
Text HLabel 7950 3600 2    50   BiDi ~ 0
SWDIO
Text HLabel 7950 3700 2    50   Input ~ 0
SWCLK
Wire Wire Line
	6050 1900 6050 1150
Wire Wire Line
	5650 2100 5650 1900
Wire Wire Line
	5650 1900 5750 1900
Wire Wire Line
	6050 1900 6050 2100
Connection ~ 5750 1900
$Comp
L Device:C C?
U 1 1 5F90BAF2
P 6600 1500
AR Path="/5F90BAF2" Ref="C?"  Part="1" 
AR Path="/5F823278/5F90BAF2" Ref="C30"  Part="1" 
F 0 "C30" H 6715 1546 50  0000 L CNN
F 1 "100n" H 6715 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6638 1350 50  0001 C CNN
F 3 "~" H 6600 1500 50  0001 C CNN
F 4 "C14663" H 6600 1500 50  0001 C CNN "LCSC Part #"
F 5 "C14663" H 6600 1500 50  0001 C CNN "LCSC"
	1    6600 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F90BF4F
P 6600 1750
AR Path="/5F90BF4F" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F90BF4F" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 6600 1500 50  0001 C CNN
F 1 "GND" H 6605 1577 50  0000 C CNN
F 2 "" H 6600 1750 50  0001 C CNN
F 3 "" H 6600 1750 50  0001 C CNN
	1    6600 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 1350 6600 1150
Connection ~ 6600 1150
Wire Wire Line
	6600 1150 7100 1150
Wire Wire Line
	6600 1750 6600 1650
$Comp
L Device:L L3
U 1 1 5F912FFB
P 9000 1150
F 0 "L3" V 9190 1150 50  0000 C CNN
F 1 "39n" V 9099 1150 50  0000 C CNN
F 2 "" H 9000 1150 50  0001 C CNN
F 3 "~" H 9000 1150 50  0001 C CNN
	1    9000 1150
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F913654
P 9350 1500
AR Path="/5F913654" Ref="C?"  Part="1" 
AR Path="/5F823278/5F913654" Ref="C31"  Part="1" 
F 0 "C31" H 9465 1546 50  0000 L CNN
F 1 "1u" H 9465 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9388 1350 50  0001 C CNN
F 3 "~" H 9350 1500 50  0001 C CNN
F 4 "C28323" H 9350 1500 50  0001 C CNN "LCSC Part #"
F 5 "C28323" H 9350 1500 50  0001 C CNN "LCSC"
	1    9350 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F913AF3
P 9850 1500
AR Path="/5F913AF3" Ref="C?"  Part="1" 
AR Path="/5F823278/5F913AF3" Ref="C32"  Part="1" 
F 0 "C32" H 9965 1546 50  0000 L CNN
F 1 "10n" H 9965 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9888 1350 50  0001 C CNN
F 3 "~" H 9850 1500 50  0001 C CNN
F 4 "C57112" H 9850 1500 50  0001 C CNN "LCSC Part #"
F 5 "C57112" H 9850 1500 50  0001 C CNN "LCSC"
	1    9850 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F913DF6
P 9350 1750
AR Path="/5F913DF6" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F913DF6" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 9350 1500 50  0001 C CNN
F 1 "GND" H 9355 1577 50  0000 C CNN
F 2 "" H 9350 1750 50  0001 C CNN
F 3 "" H 9350 1750 50  0001 C CNN
	1    9350 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F914186
P 9850 1750
AR Path="/5F914186" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F914186" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 9850 1500 50  0001 C CNN
F 1 "GND" H 9855 1577 50  0000 C CNN
F 2 "" H 9850 1750 50  0001 C CNN
F 3 "" H 9850 1750 50  0001 C CNN
	1    9850 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 1150 9350 1150
Wire Wire Line
	9350 1150 9350 1350
Wire Wire Line
	9350 1150 9850 1150
Wire Wire Line
	9850 1150 9850 1350
Connection ~ 9350 1150
Wire Wire Line
	8850 1150 8600 1150
Connection ~ 8600 1150
Wire Wire Line
	9350 1650 9350 1750
Wire Wire Line
	9850 1650 9850 1750
Wire Wire Line
	6150 2100 6150 2000
Wire Wire Line
	6150 2000 10200 2000
Wire Wire Line
	10200 2000 10200 1150
Wire Wire Line
	10200 1150 9850 1150
Connection ~ 9850 1150
$Comp
L Device:R R?
U 1 1 5F926F17
P 4750 3500
AR Path="/5F926F17" Ref="R?"  Part="1" 
AR Path="/5F823278/5F926F17" Ref="R7"  Part="1" 
F 0 "R7" V 4850 3350 50  0000 L CNN
F 1 "47" V 4850 3550 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4680 3500 50  0001 C CNN
F 3 "~" H 4750 3500 50  0001 C CNN
	1    4750 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	4900 3500 5150 3500
$Comp
L power:GND #PWR?
U 1 1 5F927EF0
P 4000 4050
AR Path="/5F927EF0" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F927EF0" Ref="#PWR0142"  Part="1" 
F 0 "#PWR0142" H 4000 3800 50  0001 C CNN
F 1 "GND" H 4005 3877 50  0000 C CNN
F 2 "" H 4000 4050 50  0001 C CNN
F 3 "" H 4000 4050 50  0001 C CNN
	1    4000 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F9281E2
P 4200 3500
AR Path="/5F9281E2" Ref="#PWR?"  Part="1" 
AR Path="/5F823278/5F9281E2" Ref="#PWR0143"  Part="1" 
F 0 "#PWR0143" H 4200 3250 50  0001 C CNN
F 1 "GND" H 4205 3327 50  0000 C CNN
F 2 "" H 4200 3500 50  0001 C CNN
F 3 "" H 4200 3500 50  0001 C CNN
	1    4200 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3950 4000 4050
Wire Wire Line
	4000 3550 4000 3500
Wire Wire Line
	4000 3500 4200 3500
$Comp
L Device:Crystal_GND24 Y1
U 1 1 5F92F6A1
P 4000 3750
F 0 "Y1" H 4050 3550 50  0000 L CNN
F 1 "25MHz/9pF" H 3800 4150 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_3225-4Pin_3.2x2.5mm_HandSoldering" H 4000 3750 50  0001 C CNN
F 3 "~" H 4000 3750 50  0001 C CNN
F 4 "C9006" H 4000 3750 50  0001 C CNN "LCSC Part #"
F 5 "C9006" H 4000 3750 50  0001 C CNN "LCSC"
	1    4000 3750
	1    0    0    -1  
$EndComp
Text HLabel 2300 2300 0    50   Input ~ 0
NRST
Wire Wire Line
	2300 2300 3650 2300
Wire Wire Line
	6550 4300 7950 4300
Text HLabel 7950 4300 2    50   Output ~ 0
SWO
$EndSCHEMATC
