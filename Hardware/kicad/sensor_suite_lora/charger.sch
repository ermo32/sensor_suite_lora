EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L modex:CN3791 U7
U 1 1 5F87617D
P 4550 3150
F 0 "U7" H 4525 3575 50  0000 C CNN
F 1 "CN3791" H 4525 3484 50  0000 C CNN
F 2 "Package_SO:SSOP-10_3.9x4.9mm_P1.00mm" H 4300 3150 50  0001 C CNN
F 3 "" H 4300 3150 50  0001 C CNN
	1    4550 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5F87A128
P 3750 3550
F 0 "R14" H 3820 3596 50  0000 L CNN
F 1 "120" H 3820 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3680 3550 50  0001 C CNN
F 3 "~" H 3750 3550 50  0001 C CNN
	1    3750 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C26
U 1 1 5F87A609
P 3750 4050
F 0 "C26" H 3865 4096 50  0000 L CNN
F 1 "220n" H 3865 4005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3788 3900 50  0001 C CNN
F 3 "~" H 3750 4050 50  0001 C CNN
F 4 "C21120" H 3750 4050 50  0001 C CNN "LCSC Part #"
F 5 "C21120" H 3750 4050 50  0001 C CNN "LCSC"
	1    3750 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3400 3750 3350
Wire Wire Line
	3750 3350 4000 3350
Wire Wire Line
	3750 3700 3750 3900
$Comp
L power:GND #PWR0135
U 1 1 5F87B231
P 3750 4400
F 0 "#PWR0135" H 3750 4150 50  0001 C CNN
F 1 "GND" H 3755 4227 50  0000 C CNN
F 2 "" H 3750 4400 50  0001 C CNN
F 3 "" H 3750 4400 50  0001 C CNN
	1    3750 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 4200 3750 4400
$Comp
L Device:R R15
U 1 1 5F87B7E8
P 5650 3550
F 0 "R15" H 5720 3596 50  0000 L CNN
F 1 "R" H 5720 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5580 3550 50  0001 C CNN
F 3 "~" H 5650 3550 50  0001 C CNN
	1    5650 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5F87D2E3
P 5650 4050
F 0 "R16" H 5720 4096 50  0000 L CNN
F 1 "R" H 5720 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5580 4050 50  0001 C CNN
F 3 "~" H 5650 4050 50  0001 C CNN
	1    5650 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0136
U 1 1 5F87D5CB
P 6700 5150
F 0 "#PWR0136" H 6700 4900 50  0001 C CNN
F 1 "GND" H 6705 4977 50  0000 C CNN
F 2 "" H 6700 5150 50  0001 C CNN
F 3 "" H 6700 5150 50  0001 C CNN
	1    6700 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4200 5650 4400
Wire Wire Line
	5650 3700 5650 3800
Wire Wire Line
	5650 3800 5250 3800
Wire Wire Line
	5250 3800 5250 3350
Wire Wire Line
	5250 3350 5050 3350
Connection ~ 5650 3800
Wire Wire Line
	5650 3800 5650 3900
Text HLabel 2450 2100 0    50   Input ~ 0
SOLAR-P
Text HLabel 2450 2650 0    50   Input ~ 0
SOLAR-N
Wire Wire Line
	5050 3050 5650 3050
Wire Wire Line
	5650 3050 5650 3400
Wire Wire Line
	5650 2100 3750 2100
Connection ~ 5650 3050
$Comp
L power:GND #PWR0137
U 1 1 5F87FF1E
P 2600 2750
F 0 "#PWR0137" H 2600 2500 50  0001 C CNN
F 1 "GND" H 2605 2577 50  0000 C CNN
F 2 "" H 2600 2750 50  0001 C CNN
F 3 "" H 2600 2750 50  0001 C CNN
	1    2600 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 2650 2600 2650
Wire Wire Line
	2600 2650 2600 2750
$Comp
L Device:C C25
U 1 1 5F88040C
P 3750 2350
F 0 "C25" H 3865 2396 50  0000 L CNN
F 1 "100n" H 3865 2305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3788 2200 50  0001 C CNN
F 3 "~" H 3750 2350 50  0001 C CNN
F 4 "C14663" H 3750 2350 50  0001 C CNN "LCSC Part #"
F 5 "C14663" H 3750 2350 50  0001 C CNN "LCSC"
	1    3750 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2200 3750 2100
Connection ~ 3750 2100
$Comp
L Device:C C24
U 1 1 5F886B50
P 2600 2350
F 0 "C24" H 2715 2396 50  0000 L CNN
F 1 "100n" H 2715 2305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2638 2200 50  0001 C CNN
F 3 "~" H 2600 2350 50  0001 C CNN
F 4 "C14663" H 2600 2350 50  0001 C CNN "LCSC Part #"
F 5 "C14663" H 2600 2350 50  0001 C CNN "LCSC"
	1    2600 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 2100 2600 2200
Connection ~ 2600 2100
Wire Wire Line
	2600 2100 2450 2100
Wire Wire Line
	2600 2500 2600 2650
Connection ~ 2600 2650
$Comp
L Device:R R12
U 1 1 5F887710
P 2950 2350
F 0 "R12" H 3020 2396 50  0000 L CNN
F 1 "1k" H 3020 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2880 2350 50  0001 C CNN
F 3 "~" H 2950 2350 50  0001 C CNN
	1    2950 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D7
U 1 1 5F8881AF
P 2950 2750
F 0 "D7" V 3050 2700 50  0000 R CNN
F 1 "DONE GRN" H 3100 2850 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2950 2750 50  0001 C CNN
F 3 "~" H 2950 2750 50  0001 C CNN
F 4 "C72043" H 2950 2750 50  0001 C CNN "LCSC Part #"
F 5 "C72043" H 2950 2750 50  0001 C CNN "LCSC"
	1    2950 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D8
U 1 1 5F888BE5
P 3350 2750
F 0 "D8" V 3450 2700 50  0000 R CNN
F 1 "CHRG RD" H 3450 2850 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3350 2750 50  0001 C CNN
F 3 "~" H 3350 2750 50  0001 C CNN
F 4 "C72044" H 3350 2750 50  0001 C CNN "LCSC Part #"
F 5 "C72044" H 3350 2750 50  0001 C CNN "LCSC"
	1    3350 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 2100 5650 3050
Wire Wire Line
	3750 2950 4000 2950
Wire Wire Line
	3750 2500 3750 2950
Wire Wire Line
	3350 2900 3350 3150
Wire Wire Line
	3350 3150 4000 3150
Wire Wire Line
	2950 2900 2950 3250
Wire Wire Line
	2950 3250 4000 3250
Wire Wire Line
	5050 2950 5250 2950
Wire Wire Line
	5250 2950 5250 1900
Wire Wire Line
	2700 1600 2700 2100
Connection ~ 2700 2100
Wire Wire Line
	2700 2100 2600 2100
$Comp
L Device:D_Schottky D9
U 1 1 5F8982BA
P 5900 1600
F 0 "D9" H 5900 1383 50  0000 C CNN
F 1 "SSB43L-E3/52T" H 5900 1474 50  0000 C CNN
F 2 "project-footprints:DO214-AA" H 5900 1600 50  0001 C CNN
F 3 "~" H 5900 1600 50  0001 C CNN
F 4 "C222519" H 5900 1600 50  0001 C CNN "LCSC Part #"
F 5 "C222519" H 5900 1600 50  0001 C CNN "LCSC"
	1    5900 1600
	-1   0    0    1   
$EndComp
Wire Wire Line
	6050 1600 6350 1600
Wire Wire Line
	6350 1600 6350 1850
Wire Wire Line
	6350 2150 6350 2350
$Comp
L power:GND #PWR0138
U 1 1 5F89B442
P 6350 2350
F 0 "#PWR0138" H 6350 2100 50  0001 C CNN
F 1 "GND" H 6355 2177 50  0000 C CNN
F 2 "" H 6350 2350 50  0001 C CNN
F 3 "" H 6350 2350 50  0001 C CNN
	1    6350 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:L L2
U 1 1 5F89BF91
P 6750 1600
F 0 "L2" V 6940 1600 50  0000 C CNN
F 1 "100u" V 6849 1600 50  0000 C CNN
F 2 "" H 6750 1600 50  0001 C CNN
F 3 "~" H 6750 1600 50  0001 C CNN
	1    6750 1600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6350 1600 6600 1600
Connection ~ 6350 1600
$Comp
L Device:R R19
U 1 1 5F89D1C1
P 7350 1600
F 0 "R19" V 7143 1600 50  0000 C CNN
F 1 "0.05" V 7234 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric_Pad1.52x3.35mm_HandSolder" V 7280 1600 50  0001 C CNN
F 3 "~" H 7350 1600 50  0001 C CNN
	1    7350 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 1600 7100 1600
Wire Wire Line
	7100 1600 7100 3150
Wire Wire Line
	7100 3150 5050 3150
Connection ~ 7100 1600
Wire Wire Line
	7100 1600 7200 1600
Wire Wire Line
	7500 1600 7650 1600
Wire Wire Line
	7650 1600 7650 3250
Wire Wire Line
	7650 3250 5050 3250
$Comp
L Device:D_Schottky D10
U 1 1 5F8A621D
P 6350 2000
F 0 "D10" V 6304 2080 50  0000 L CNN
F 1 "SSB43L-E3/52T" V 6395 2080 50  0000 L CNN
F 2 "project-footprints:DO214-AA" H 6350 2000 50  0001 C CNN
F 3 "~" H 6350 2000 50  0001 C CNN
F 4 "C222519" H 6350 2000 50  0001 C CNN "LCSC Part #"
F 5 "C222519" H 6350 2000 50  0001 C CNN "LCSC"
	1    6350 2000
	0    1    1    0   
$EndComp
$Comp
L Device:C C28
U 1 1 5F8A6DE4
P 8000 2000
F 0 "C28" H 8115 2046 50  0000 L CNN
F 1 "4u7" H 8115 1955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8038 1850 50  0001 C CNN
F 3 "~" H 8000 2000 50  0001 C CNN
F 4 "C1779" H 8000 2000 50  0001 C CNN "LCSC Part #"
F 5 "C1779" H 8000 2000 50  0001 C CNN "LCSC"
	1    8000 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 1850 8000 1600
Wire Wire Line
	8000 1600 7650 1600
Connection ~ 7650 1600
Wire Wire Line
	8700 1850 8700 1600
Wire Wire Line
	8700 1600 8000 1600
Connection ~ 8000 1600
Wire Wire Line
	8000 2150 8000 2350
Wire Wire Line
	8700 2150 8700 2350
$Comp
L power:GND #PWR0139
U 1 1 5F8AB534
P 8000 2350
F 0 "#PWR0139" H 8000 2100 50  0001 C CNN
F 1 "GND" H 8005 2177 50  0000 C CNN
F 2 "" H 8000 2350 50  0001 C CNN
F 3 "" H 8000 2350 50  0001 C CNN
	1    8000 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0140
U 1 1 5F8ABD66
P 8700 2350
F 0 "#PWR0140" H 8700 2100 50  0001 C CNN
F 1 "GND" H 8705 2177 50  0000 C CNN
F 2 "" H 8700 2350 50  0001 C CNN
F 3 "" H 8700 2350 50  0001 C CNN
	1    8700 2350
	1    0    0    -1  
$EndComp
Text HLabel 9350 1600 2    50   UnSpc ~ 0
BAT-P
Wire Wire Line
	9350 1600 9250 1600
Connection ~ 8700 1600
$Comp
L modex:FS312F-G U8
U 1 1 5F89F52F
P 7750 4450
F 0 "U8" H 7750 4775 50  0000 C CNN
F 1 "FS312F-G" H 7750 4684 50  0000 C CNN
F 2 "" H 7750 4350 50  0001 C CNN
F 3 "" H 7750 4350 50  0001 C CNN
F 4 "C82736" H 7750 4450 50  0001 C CNN "LCSC"
F 5 "C82736" H 7750 4450 50  0001 C CNN "LCSC Part #"
	1    7750 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R17
U 1 1 5F89FA6C
P 6700 4100
F 0 "R17" H 6770 4146 50  0000 L CNN
F 1 "100" H 6770 4055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6630 4100 50  0001 C CNN
F 3 "~" H 6700 4100 50  0001 C CNN
	1    6700 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C27
U 1 1 5F8A1144
P 6700 4600
F 0 "C27" H 6815 4646 50  0000 L CNN
F 1 "100n" H 6815 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6738 4450 50  0001 C CNN
F 3 "~" H 6700 4600 50  0001 C CNN
F 4 "C14663" H 6700 4600 50  0001 C CNN "LCSC Part #"
F 5 "C14663" H 6700 4600 50  0001 C CNN "LCSC"
	1    6700 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4250 6700 4350
Wire Wire Line
	7400 4350 6700 4350
Connection ~ 6700 4350
Wire Wire Line
	6700 4350 6700 4450
Wire Wire Line
	7400 4450 7100 4450
Wire Wire Line
	7100 4450 7100 5000
Wire Wire Line
	7100 5000 6700 5000
Wire Wire Line
	6700 5000 6700 4750
$Comp
L Device:R R18
U 1 1 5F8A8433
P 7300 4750
F 0 "R18" H 7370 4796 50  0000 L CNN
F 1 "2k" H 7370 4705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7230 4750 50  0001 C CNN
F 3 "~" H 7300 4750 50  0001 C CNN
	1    7300 4750
	1    0    0    -1  
$EndComp
Connection ~ 7100 5000
Text HLabel 9300 3900 2    50   UnSpc ~ 0
BAT-N
Wire Wire Line
	9300 3900 8800 3900
Wire Wire Line
	8800 3900 8800 4050
Wire Wire Line
	8500 4250 8300 4250
Wire Wire Line
	8300 4250 8300 4350
Wire Wire Line
	8300 4350 8100 4350
Wire Wire Line
	8300 4550 8300 4650
Wire Wire Line
	8300 4650 8500 4650
Wire Wire Line
	8100 4550 8300 4550
Wire Wire Line
	8800 5000 8800 4850
Wire Wire Line
	7100 5000 7300 5000
Wire Wire Line
	7300 4550 7400 4550
Wire Wire Line
	9250 1600 9250 3700
Wire Wire Line
	9250 3700 6700 3700
Wire Wire Line
	6700 3700 6700 3950
Connection ~ 9250 1600
Wire Wire Line
	9250 1600 8700 1600
Wire Wire Line
	6700 5000 6700 5150
Connection ~ 6700 5000
Wire Wire Line
	7300 4550 7300 4600
Wire Wire Line
	7300 4900 7300 5000
Connection ~ 7300 5000
Wire Wire Line
	7300 5000 8800 5000
$Comp
L Device:R R13
U 1 1 5F8DCF3B
P 3350 2350
F 0 "R13" H 3420 2396 50  0000 L CNN
F 1 "1k" H 3420 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3280 2350 50  0001 C CNN
F 3 "~" H 3350 2350 50  0001 C CNN
	1    3350 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2100 2950 2100
Wire Wire Line
	2950 2200 2950 2100
Connection ~ 2950 2100
Wire Wire Line
	2950 2100 3350 2100
Wire Wire Line
	3350 2200 3350 2100
Connection ~ 3350 2100
Wire Wire Line
	3350 2100 3750 2100
Wire Wire Line
	2950 2500 2950 2600
Wire Wire Line
	3350 2500 3350 2600
$Comp
L power:GND #PWR0141
U 1 1 5F8E931B
P 5650 4400
F 0 "#PWR0141" H 5650 4150 50  0001 C CNN
F 1 "GND" H 5655 4227 50  0000 C CNN
F 2 "" H 5650 4400 50  0001 C CNN
F 3 "" H 5650 4400 50  0001 C CNN
	1    5650 4400
	1    0    0    -1  
$EndComp
$Comp
L modex:FS8205A U9
U 1 1 5F8FD1DB
P 8800 4450
F 0 "U9" V 8846 4306 50  0000 R CNN
F 1 "FS8205A" V 8755 4306 50  0000 R CNN
F 2 "" H 9400 3950 50  0001 C CNN
F 3 "" H 9400 3950 50  0001 C CNN
	1    8800 4450
	0    -1   -1   0   
$EndComp
Text HLabel 9350 1400 2    50   Output ~ 0
PBAT
Wire Wire Line
	9250 1600 9250 1400
Wire Wire Line
	9250 1400 9350 1400
Wire Wire Line
	5550 1600 5750 1600
Wire Wire Line
	4950 1600 2700 1600
$Comp
L modex:CEM4435A T1
U 1 1 5F8ACE7A
P 5250 1600
F 0 "T1" V 5492 1600 50  0000 C CNN
F 1 "CEM4435A" V 5401 1600 50  0000 C CNN
F 2 "" H 5250 1600 50  0001 C CNN
F 3 "" H 5250 1600 50  0001 C CNN
	1    5250 1600
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C29
U 1 1 5F9216F8
P 8700 2000
F 0 "C29" H 8815 2046 50  0000 L CNN
F 1 "10u" H 8815 1955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8738 1850 50  0001 C CNN
F 3 "~" H 8700 2000 50  0001 C CNN
F 4 "C15850" H 8700 2000 50  0001 C CNN "LCSC Part #"
F 5 "C15850" H 8700 2000 50  0001 C CNN "LCSC"
	1    8700 2000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
